#include <iostream>

#define SIZE 8
using namespace std;

int tab[SIZE * SIZE];
int counter = 0;

enum dir {
    RIGHT, DOWN, LEFT, UP
};

class Point {
private:
    int x, y;
public:
    Point(int _x, int _y) : x(_x), y(_y) { };

    int getX() {
        return this->x;
    }

    int getY() {
        return this->y;
    }
};

Point *points[SIZE * SIZE];

void move(dir where, int ile, int *x, int *y, int *aux_cnt, int *ile_mniej) {
    if (*ile_mniej == SIZE)
        return;

    switch (where) {
        case RIGHT:
            for (int i = 0; i < ile; ++i)
                points[counter++] = new Point(*x, (*y)++);
            break;
        case LEFT:
            for (int i = 0; i < ile; ++i)
                points[counter++] = new Point(*x, (*y)--);
            break;
        case UP:
            for (int i = 0; i < ile; ++i)
                points[counter++] = new Point((*x)--, *y);
            break;
        case DOWN:
            for (int i = 0; i < ile; ++i)
                points[counter++] = new Point((*x)++, *y);
            break;
    }

    if (++(*aux_cnt) == 2) {
        *aux_cnt = 0;
        (*ile_mniej)++;
    }
}

void go_clockwise() {
    int x = 0, y = 0;
    int ile_mniej = 1;
    int aux_cnt = -1;
    while (ile_mniej < SIZE) {
        move(RIGHT, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej);
        move(DOWN, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej);
        move(LEFT, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej);
        move(UP, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej);
    }
    points[counter] = new Point(x, y);
}

int main() {
    go_clockwise();
    int x =1;
    for (int i = 0; i < SIZE * SIZE; ++i) {
        cout << x++ << ": " << points[i]->getX() << "," << points[i]->getY() << endl;
    }
    //delete []points;
    return 0;
}
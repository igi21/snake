#include <iostream>
#define SIZE 8
using namespace std;

enum side {
    RIGHT, DOWN, LEFT, UP
};

enum dir {
    CLOCKWISE, COUNTERCLOCKWISE
};

class Point {
private:
    int x, y;
public:
    Point(int _x, int _y) : x(_x), y(_y) { };
    int getX() {
        return this->x;
    }
    int getY() {
        return this->y;
    }
};

void move(side _side, dir where, int ile, int *x, int *y, int *aux_cnt, int *ile_mniej, int* counter, Point** points) {
    if (where == CLOCKWISE) {
        if (*ile_mniej == SIZE) //wariant przy ruchu zgodnym ze wskazowkami zegara
            return; //przeciwdziala wejsciu do funkcji, gdy tablica juz wypelniona
        if (++(*aux_cnt) == 2) {
            *aux_cnt = 0;
            (*ile_mniej)++;
        }
    } else {    //przeciwnie do wsk. zegara
        if (*ile_mniej == 0)
            return;
        if (++(*aux_cnt) == 2) {
            *aux_cnt = 0;
            (*ile_mniej)--;
        }
    }

    switch (_side) {//ruch w dana strone(prawo,lewo itd) o zadana liczbe miejsc
        case RIGHT:
            for (int i = 0; i < ile; ++i)
                points[(*counter)++] = new Point(*x, (*y)++);
            break;
        case LEFT:
            for (int i = 0; i < ile; ++i)
                points[(*counter)++] = new Point(*x, (*y)--);
            break;
        case UP:
            for (int i = 0; i < ile; ++i)
                points[(*counter)++] = new Point((*x)--, *y);
            break;
        case DOWN:
            for (int i = 0; i < ile; ++i)
                points[(*counter)++] = new Point((*x)++, *y);
            break;
    }

}

//poruszaj sie zgodnie z ruchem wsk. zegara
Point **go_clockwise() {
    int counter = 0;
    Point **points = new Point*[SIZE * SIZE];//tablica z pktami
    int x = 0, y = 0;   //punkt startowy
    int ile_mniej = 1;  //dlugosc ruchu = SIZE - ile_mniej
    int aux_cnt = -1;   //zmienna pomocnicza: if aux_cnt==2 then zmien wartosc ile_mniej(bo dlugosc ruchu zmienia sie co 2 obiegi)
    while (ile_mniej < SIZE) {
        move(RIGHT, CLOCKWISE, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej, &counter, points);
        move(DOWN, CLOCKWISE, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej, &counter, points);
        move(LEFT, CLOCKWISE, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej, &counter, points);
        move(UP, CLOCKWISE, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej, &counter, points);
    }
    points[counter] = new Point(x, y);  //dodanie ostatniego pktu
    return points;
}

Point **go_counter() {
    int counter = 0;
    Point **points = new Point*[SIZE * SIZE];

    int x = 4, y = 3;
    int ile_mniej = SIZE - 1;
    int aux_cnt = 0;
    while (ile_mniej > 0) {
        move(UP, COUNTERCLOCKWISE, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej, &counter, points);
        move(RIGHT, COUNTERCLOCKWISE, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej, &counter, points);
        move(DOWN, COUNTERCLOCKWISE, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej, &counter, points);
        move(LEFT, COUNTERCLOCKWISE, SIZE - ile_mniej, &x, &y, &aux_cnt, &ile_mniej, &counter, points);
    }
    for (int i = 0; i < SIZE; ++i)  //dodanie ostatniej kolumny
        points[counter++] = new Point(x++, y);
    return points;
}

int main() {
    //testy
    Point** points = go_clockwise();
    int x = 1;
    for (int i = 0; i < SIZE * SIZE; ++i) {
        cout << x++ << ": " << points[i]->getX() << "," << points[i]->getY() << endl;
    }

    cout << endl;
    Point** points2 = go_counter();
    int x2 = 1;
    for (int i = 0; i < SIZE * SIZE; ++i) {
        cout << x2++ << ": " << points2[i]->getX() << "," << points2[i]->getY() << endl;
    }
    delete[] points;
    delete[] points2;
    return 0;
}